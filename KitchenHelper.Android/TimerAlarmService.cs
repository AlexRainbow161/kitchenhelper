﻿using System;
using KitchenHelper.Droid;
using KitchenHelper.Services;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Threading.Tasks;
using Android.Media;
using System.Threading;

[assembly: Dependency(typeof(TimerAlarmService))]
namespace KitchenHelper.Droid
{
    public class TimerAlarmService : ITimerNotification
    {
        public async Task AlarmAsync(CancellationToken cancellationToken)
        {
            await Task.Run(async () =>
            {
                ToneGenerator toneGenerator = new ToneGenerator(Stream.Alarm, 100);

                var endTime = DateTime.Now.Add(TimeSpan.FromSeconds(10));

                while ((endTime - DateTime.Now).TotalMilliseconds > 0 && !cancellationToken.IsCancellationRequested)
                {
                    toneGenerator.StartTone(Tone.CdmaAlertCallGuard, 1000);
                    await Task.Delay(1000);
                }
                
                await Task.Delay(200).ContinueWith((o) => toneGenerator.Release());

            });
            
        }

        public async Task SpeachAsync(string textToSpeach)
        {
            await TextToSpeech.SpeakAsync(textToSpeach);
        }
    }
}
