﻿using System;
using System.Linq;
using System.Diagnostics;
using Xamarin.Forms;

namespace KitchenHelper.ViewModels
{
    public class NewTimerViewModel : BaseViewModel
    {
        public Command Save { get; }
        public Command Cancel { get; }
        public int[] PickerValuesHours => Enumerable.Range(0, 24).ToArray();
        public int[] PickerValuesMinutes => Enumerable.Range(0, 60).ToArray();
        public int[] PickerValuesSeconds => Enumerable.Range(0, 60).ToArray();

        public NewTimerViewModel()
        {
            Title = "Добавить таймер";
            Save = new Command(OnSave, ValidateSave);
            Cancel = new Command(OnCancel);
            PropertyChanged +=
                (_, __) => Save.ChangeCanExecute();
        }

        private string _title;

        public string TimerTittle
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private int _hours;
        private int _minutes;
        private int _seconds;

        public int Hours
        {
            get { return _hours; }
            set { SetProperty(ref _hours, value); }
        }

        public int Minutes
        {
            get { return _minutes; }
            set { SetProperty(ref _minutes, value); }
        }

        public int Seconds
        {
            get { return _seconds; }
            set { SetProperty(ref _seconds, value); }
        }

        private bool _speachOnEnd;

        public bool SpeachOnEnd
        {
            get { return _speachOnEnd; }
            set { SetProperty(ref _speachOnEnd, value); }
        }

        private string _speachText;

        public string SpeachText
        {
            get { return _speachText; }
            set { SetProperty(ref _speachText, value); }
        }

        private TimeSpan GetDuration() => new TimeSpan(Hours, Minutes, Seconds);

        private bool ValidateSave()
        {
            return !string.IsNullOrEmpty(_title?.Trim())
                && GetDuration().TotalSeconds > 0;
        }

        public async void OnSave()
        {
            try
            {             
                await DataStore.AddItemAsync(new Models.Timer()
                {
                    Duration = GetDuration(),
                    Title = TimerTittle,
                    SpeachOnEnd = SpeachOnEnd,
                    SpeachText = SpeachText
                });

                await Shell.Current.GoToAsync("..");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public async void OnCancel()
        {
            await Shell.Current.GoToAsync("..");
        }
    }
}
