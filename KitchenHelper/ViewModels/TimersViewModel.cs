﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using KitchenHelper.Models;
using KitchenHelper.Views;
using Xamarin.Forms;

namespace KitchenHelper.ViewModels
{
    public class TimersViewModel : BaseViewModel
    {
        public ObservableCollection<Timer> Timers { get; }
        public Command LoadTimersCommand { get; }
        public Command AddTimer { get; }
        public Command<Timer> TapTimer { get; }

        public TimersViewModel()
        {
            Title = "Таймеры";
            Timers = new ObservableCollection<Timer>();
            LoadTimersCommand = new Command(async () => await ExecuteLoadTimers());
            TapTimer = new Command<Timer>(OnTimerSelected);
            AddTimer = new Command(OnAddTimer);
        }

        async Task ExecuteLoadTimers()
        {
            IsBusy = true;

            try
            {
                Timers.Clear();
                var timers = await DataStore.GetItemsAsync(true);
                foreach (var timer in timers)
                {
                    Timers.Add(timer);
                }
                    
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        async void OnAddTimer()
        {
            await Shell.Current.GoToAsync(nameof(NewTimerPage));
        }

        public void OnAppearing()
        {
            IsBusy = true;
        }

        async void OnTimerSelected(Timer timer)
        {
            if (timer == null) return;
            await Shell.Current.GoToAsync($"{nameof(TimerDetailsPage)}?{nameof(TimerDetailsViewModel.TimerId)}={timer.Id}");
        }
    }
}
