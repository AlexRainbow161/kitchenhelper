﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using KitchenHelper.Models;
using KitchenHelper.Services;
using Xamarin.Forms;
using System.Threading;

namespace KitchenHelper.ViewModels
{
    [QueryProperty(nameof(TimerId), nameof(TimerId))]
    public class TimerDetailsViewModel : BaseViewModel
    {
        private string timerId;
        private KitchenHelper.Models.Timer timer;
        private TimeSpan duration;
        private bool isButtonEnabled = true;
        private ITimerNotification timerNotification;
        private CancellationTokenSource cancellationTokenSource;

        public Command RunTimerCommand { get; }
        public Command StopTimerCommand { get; }
        public Command DeleteTimerCommand { get; }
             
        public TimerDetailsViewModel()
        {
            timerNotification = DependencyService.Get<ITimerNotification>();
            RunTimerCommand = new Command(OnTimerRunClicked);
            StopTimerCommand = new Command(OnTimerStopClicked);
            DeleteTimerCommand = new Command(OnItemDeleteClicked, ValidateCanDelete);
            this.PropertyChanged +=
                (_, __) => DeleteTimerCommand.ChangeCanExecute();
        }

        public string TimerId
        {
            get { return timerId; }
            set
            {
                timerId = value;
                LoadTimer(int.Parse(value));
            }
        }
                
        public TimeSpan Duration
        {
            get { return duration; }
            set { SetProperty(ref duration, value); }
        }

        private bool _isSpeachOnEnd;

        public bool IsSpeachOnEnd
        {
            get { return _isSpeachOnEnd; }
            set { SetProperty(ref _isSpeachOnEnd, value); }
        }

        private string _speachText;

        public string SpeachText
        {
            get { return _speachText; }
            set { SetProperty(ref _speachText, value); }
        }

        public bool IsButtonEnable
        {
            get { return isButtonEnabled; }
            set { SetProperty(ref isButtonEnabled, value); }
        }

        public async void LoadTimer(int timerId)
        {
            try
            {
                timer = await DataStore.GetItemAsync(timerId);
                Title = timer.Title;
                Duration = timer.Duration;
                IsSpeachOnEnd = timer.SpeachOnEnd;
                SpeachText = string.IsNullOrEmpty(timer.SpeachText) ? timer.Title : timer.SpeachText;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private bool ValidateCanDelete()
        {
            return IsButtonEnable;
        }

        public async void OnItemDeleteClicked()
        {
            try
            {
                await DataStore.DeleteItemAsync(timer.Id);
                await Shell.Current.GoToAsync("..");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public void OnTimerStopClicked() => cancellationTokenSource?.Cancel();

        public async void OnTimerRunClicked()
        {
            IsButtonEnable = false;
            cancellationTokenSource = new CancellationTokenSource();

            await Task.Run(async () =>
            {
                bool end = false;
                DateTime total = DateTime.Now.Add(timer.Duration);

                System.Timers.Timer t = new System.Timers.Timer(timer.Duration.TotalMilliseconds)
                {
                    Enabled = true,
                    AutoReset = false
                };

                System.Timers.ElapsedEventHandler event_handled = (s, e) =>
                {
                    end = true;
                };

                t.Elapsed += event_handled;

                t.Start();

                while (!end)
                {
                    if (cancellationTokenSource.IsCancellationRequested)
                    {
                        t.Stop();
                        break;
                    }

                    await Task.Delay(200);
                    Duration = total - DateTime.Now;
                }

                if (!cancellationTokenSource.IsCancellationRequested)
                {
                    if (timer.SpeachOnEnd)
                        await timerNotification.SpeachAsync(SpeachText);

                    await timerNotification.AlarmAsync(cancellationTokenSource.Token);
                }
                    

                t.Elapsed -= event_handled;

                t.Dispose();

                Duration = timer.Duration;
                cancellationTokenSource.Dispose();
            });

            IsButtonEnable = true;
        }
    }
}
