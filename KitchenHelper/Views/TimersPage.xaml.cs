﻿using System;
using System.Collections.Generic;
using KitchenHelper.ViewModels;
using Xamarin.Forms;

namespace KitchenHelper.Views
{
    public partial class TimersPage : ContentPage
    {
        TimersViewModel _timersViewModel;

        public TimersPage()
        {
            InitializeComponent();
            BindingContext = _timersViewModel = new TimersViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _timersViewModel.OnAppearing();
        }
    }
}
