﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace KitchenHelper.Services
{
    public interface ITimerNotification
    {
        /// <summary>
        /// Озвучивает посланый текст
        /// </summary>
        /// <param name="textToSpeach">Текст который надо озвучить</param>
        /// <returns></returns>
        Task SpeachAsync(string textToSpeach);

        /// <summary>
        /// Включает звуковое оповещение таймера
        /// </summary>
        /// <returns></returns>
        Task AlarmAsync(CancellationToken cancellationToken);
    }
}
