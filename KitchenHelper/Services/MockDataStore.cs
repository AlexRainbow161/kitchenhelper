﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KitchenHelper.Models;
using KitchenHelper.Providers;
using Xamarin.Forms;

namespace KitchenHelper.Services
{
    public class TimersDataService : IDataStore<Timer>
    {
        AppDatabaseContext _db;

        public TimersDataService()
        {
            _db = DependencyService.Get<AppDatabaseContext>();
        }

        public async Task<bool> AddItemAsync(Timer item)
        {
            _db.Timers.Add(item);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> UpdateItemAsync(Timer item)
        {
            var oldItem = _db.Timers.Where((Timer arg) => arg.Id == item.Id).FirstOrDefault();
            _db.Timers.Update(oldItem);

            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteItemAsync(int id)
        {
            var oldItem = _db.Timers.Where((Timer arg) => arg.Id == id).FirstOrDefault();
            _db.Timers.Remove(oldItem);

            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<Timer> GetItemAsync(int id)
        {
            return await Task.FromResult(_db.Timers.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Timer>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(_db.Timers.ToList());
        }
    }
}