﻿using System;
namespace KitchenHelper.Services
{
    public interface IPathService
    {
        string GetPath(string dbFileName);
    }
}
