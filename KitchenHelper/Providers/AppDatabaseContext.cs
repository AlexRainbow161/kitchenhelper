﻿using System;
using Xamarin.Forms;
using KitchenHelper.Services;
using Microsoft.EntityFrameworkCore;

namespace KitchenHelper.Providers
{
    public class AppDatabaseContext: DbContext
    {
        private string _dbPath;

        private const string DBFILENAME = "KitchenHelper.db";

        public DbSet<Models.Timer> Timers { get; set; }

        public AppDatabaseContext()
        {
            IPathService pathService = DependencyService.Get<IPathService>();

            _dbPath = pathService.GetPath(DBFILENAME);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_dbPath}");
        }
    }
}
