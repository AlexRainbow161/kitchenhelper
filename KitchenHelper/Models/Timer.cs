﻿using System;
namespace KitchenHelper.Models
{
    public class Timer
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public TimeSpan Duration { get; set; }

        /// <summary>
        /// Нужно озвучивать по завершению или нет.
        /// </summary>
        public bool SpeachOnEnd { get; set; }
        /// <summary>
        /// Текст который надо озвучить по завершению.
        /// </summary>
        public string SpeachText { get; set; }
    }
}
