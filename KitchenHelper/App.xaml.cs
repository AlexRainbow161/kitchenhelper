﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using KitchenHelper.Services;
using KitchenHelper.Providers;
using KitchenHelper.Views;
using Microsoft.EntityFrameworkCore;

namespace KitchenHelper
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<TimersDataService>();
            DependencyService.Register<AppDatabaseContext>();

            CheckDb();

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private void CheckDb()
        {
            using (var db = new AppDatabaseContext())
            {
                db.Database.EnsureCreated();
                db.Database.Migrate();
            }
        }
    }
}
