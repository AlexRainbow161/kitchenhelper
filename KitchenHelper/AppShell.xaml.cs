﻿using System;
using System.Collections.Generic;
using KitchenHelper.ViewModels;
using KitchenHelper.Views;
using Xamarin.Forms;

namespace KitchenHelper
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();            
            Routing.RegisterRoute(nameof(TimerDetailsPage), typeof(TimerDetailsPage));
            Routing.RegisterRoute(nameof(NewTimerPage), typeof(NewTimerPage));
        }

    }
}
